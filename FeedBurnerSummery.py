import requests
import xml.etree.ElementTree as ET
import re
from nltk.stem import WordNetLemmatizer
from bs4 import BeautifulSoup
import RAKE
import wikipediaapi

lemmataizer = WordNetLemmatizer()
rake = RAKE.Rake(RAKE.SmartStopList())

# get RSS feed and convert to XML
feed = requests.get("http://feeds.feedburner.com/TechCrunch/").content
root = ET.fromstring(feed)

for article in root.findall('channel/item'):
    title = article.find('title').text
    link = article.find('link').text
    description = article.find('description').text
    content = BeautifulSoup(article.find('{http://purl.org/rss/1.0/modules/content/}encoded').text, 'html.parser').getText()

    # stem the words to help Rake with the count
    article_text = re.sub(r'\w+', lambda m: lemmataizer.lemmatize(m[0]), f"{title}. {description}. {content}")
    keywords = {key for key, val in rake.run(article_text, minFrequency=3)}
    wiki = wikipediaapi.Wikipedia('en')
    filtered_keywords = {key for key in keywords if wiki.page(key).exists()}

    print(f"Title: {title} \n Link: {link} \n")
    print(f"My keywords: \n {filtered_keywords} \n")
    print(f"Article keywords: \n {[category.text for category in article.findall('category')]} \n")
    print(f"wikipedea filtered: \n {keywords - filtered_keywords} ")
    print('\n\n\n')
